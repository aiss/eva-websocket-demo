
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

http.listen(8001, function(){
  console.log('listening on *:8001');
});


var pickupList = [];
var ledList = [];


io.on('connection', function(socket) {

    console.log('>>> socket client connected, id: ', socket.id);

    socket.on('disconnect', function() {
        console.log('<<< socket client disconnected, id: ', socket.id);
    });

    socket.on('hello', function(msg) {
        console.log('> hello from', msg);
    });


    // controlled by HW
    socket.on('hw-pickup-state', function(data) {
        console.log('Pickup state changed by HW');
        console.log('hw-pickup-state invoked: ', data.portList);

        pickupList = data.portList;

        // broadcast to clients
        socket.broadcast.emit('pickup-state', data);
        console.log('Pickup state sent to clients');
        console.log('');
    });

    // controlled by clients
    socket.on('led-state', function(data) {
        console.log('LED state changed by client');
        console.log('led-state invoked: ', data.portList);

        ledList = data.portList;

        // send to HW
        socket.broadcast.emit('hw-led-state', data);
        console.log('LED state sent to HW');
        console.log('');
    });

    // set pickup init state on clients;
    socket.emit('pickup-state', { portList: pickupList });

    // set led init state on HW;
    socket.emit('hw-led-state', { portList: ledList });

});

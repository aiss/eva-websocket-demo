# EVA websocket demo #

### requirements ###
* node
* python (just for serving htmls...)

### preparation ###
```
#!javascript

cd ws-server
npm install

```

### usage ###
* open a terminal, run socket server (localhost:8001)
```
#!javascript

cd ws-server
./start-server.sh

```

* open another terminal, run 'web server' (localhost:8000)
```
#!javascript

cd clients
./start-webserver.sh

```

* open a browser windows with URL: http://localhost:8000/eva-sim.html
* open another browser windows with URL: http://localhost:8000/hw-sim.html
* click on ports, try to stop/restart socket server, try to reload webpages, check console logs of both terminal and browsers